/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef INCLUDE_POWER_MANAGER_POWER_MANAGER_H_
#define INCLUDE_POWER_MANAGER_POWER_MANAGER_H_

#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include "apl_msgs/PowerCommand.h"
#include "apl_msgs/PowerStatus.h"
#include "ros/ros.h"

class PowerManager {
 public:
  PowerManager();
  // We have a mutex as a member variable, so copy contstructor is forbidden.
  PowerManager(const PowerManager&) = delete;
  ~PowerManager() = default;

  void run();

 private:
  // Query state of controlled pins and publish status message
  void timerCallback(const ros::TimerEvent&);

  // Respond to incoming command
  void commandCallback(const apl_msgs::PowerCommand& cmd);

  // Setup all ROS parameters
  void setupParams();
  // Do all the wiringPi setup tasks
  void setupPins();

  // Descriptive names for channels that the power manager is controlling
  std::set<std::string> channels_;

  // Map from descriptive name to wiringPi pin number
  // The wiringPi pin number will differ from the physical pin number on the
  // ODroid; use `gpio readall -a` to determine the mapping.
  std::map<std::string, int> wiringpi_pins_;

  // Per-pin configuration of whether it needs to be set high/low to
  // provide power to the attached device. Useful because some pins
  // have pull-up resistors and we don't want potential transient state
  // changes when the ODroid is booting -- easier to just flip their
  // logical sense.
  std::map<std::string, bool> on_is_high_;

  std::map<std::string, bool> initial_state_;

  // Rate at which to query the DIO pin
  double rate_hz_;

  // Mutex protecting pin read/writes.
  std::mutex pin_mutex_;

  ros::NodeHandle nh_;
  ros::Timer status_timer_;
  ros::Publisher status_pub_;
  ros::Subscriber command_sub_;
};

#endif  //  INCLUDE_POWER_MANAGER_POWER_MANAGER_H_
