/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "power_manager/power_manager.h"

#include <wiringpi2/wiringPi.h>

#include "apl_msgs/PowerCommand.h"
#include "apl_msgs/PowerStatus.h"
#include "ros/ros.h"

PowerManager::PowerManager() {
  nh_ = ros::NodeHandle("~");

  setupParams();

  status_pub_ = nh_.advertise<apl_msgs::PowerStatus>("status", 10);

  setupPins();

  command_sub_ =
      nh_.subscribe("command", 1, &PowerManager::commandCallback, this);
}

void PowerManager::setupPins() {
  wiringPiSetup();
  // QUESTION: At startup, should it ensure that all power channels are OFF?
  //  (is the pullup/pulldown resistor enough, or do we want to drive it
  //  always?)
  // This seems to set all of them high, even ones that default to pulldown.
  for (const auto& channel : channels_) {
    const int wiringpi_pin = wiringpi_pins_[channel];
    // OUTPUT mode does let us both read and write pin status
    // However, it drives the pins to high.
    pinMode(wiringpi_pin, OUTPUT);
    // This immediately drives them to the desired state, but I don't
    // like this -- it makes the on_is_high parameter kinda useless,
    // since if it's `true` there WILL be a blip.
    if (on_is_high_[channel]) {
      digitalWrite(wiringpi_pin, LOW);
    } else {
      digitalWrite(wiringpi_pin, HIGH);
    }

    if (initial_state_[channel]) {
      if (on_is_high_[channel]) {
        digitalWrite(wiringpi_pin, HIGH);
      } else {
        digitalWrite(wiringpi_pin, LOW);
      }
    }
  }
}

void PowerManager::setupParams() {
  nh_.getParam("rate_hz", rate_hz_);
  std::vector<int> wiringpi_pins;
  nh_.getParam("wiringpi_pins", wiringpi_pins);
  std::vector<std::string> channel_names;
  nh_.getParam("channel_names", channel_names);
  std::vector<int> on_is_high;
  nh_.getParam("on_is_high", on_is_high);

  std::vector<int> initial_state;
  nh_.getParam("initial_state", initial_state);

  int num_pins = wiringpi_pins.size();
  if (num_pins == 0) {
    ROS_FATAL("Driver requires at least one pin to be configured.");
    ros::shutdown();
  }
  if (channel_names.size() != num_pins) {
    ROS_FATAL("channel_names array must match wiringpi_pins array length.");
    ros::shutdown();
  }
  if (on_is_high.size() != num_pins) {
    ROS_FATAL("on_is_high array must match wiringpi_pins array length.");
    ros::shutdown();
  }
  if (initial_state.size() != num_pins) {
    ROS_FATAL("initial_state array must match wiringpi_pins array length.");
    ros::shutdown();
  }

  // Store into maps for easy use
  for (int ii = 0; ii < num_pins; ii++) {
    std::string channel = channel_names[ii];
    channels_.insert(channel);
    wiringpi_pins_.insert(
        std::pair<std::string, int>(channel, wiringpi_pins[ii]));
    on_is_high_.insert(std::pair<std::string, bool>(channel, on_is_high[ii]));
    initial_state_.insert(
        std::pair<std::string, bool>(channel, initial_state[ii]));
  }
}

void PowerManager::run() {
  float duration = 1.0 / rate_hz_;
  status_timer_ = nh_.createTimer(ros::Duration(duration),
                                  &PowerManager::timerCallback, this);

  ros::spin();
}

void PowerManager::commandCallback(const apl_msgs::PowerCommand& cmd) {
  const std::lock_guard<std::mutex> lg(pin_mutex_);
  int num_cmd_channels = cmd.channels.size();
  if (cmd.commands.size() != num_cmd_channels) {
    ROS_WARN("Size mismatch between pins and commands in message. Ignoring.");
    return;
  }
  for (int ii = 0; ii < num_cmd_channels; ii++) {
    std::string channel = cmd.channels[ii];
    if (channels_.count(channel) == 0) {
      ROS_WARN_STREAM(
          "Command received for channel "
          << channel
          << ", but it's not in the power_manager's configuration. Ignoring.");
      continue;
    }

    int wiringpi_pin = wiringpi_pins_[channel];
    bool command = cmd.commands[ii];
    if (on_is_high_[channel]) {
      if (cmd.ON == command) {
        digitalWrite(wiringpi_pin, HIGH);
      } else {
        digitalWrite(wiringpi_pin, LOW);
      }
    } else {
      if (cmd.ON == command) {
        digitalWrite(wiringpi_pin, LOW);
      } else {
        digitalWrite(wiringpi_pin, HIGH);
      }
    }
  }
}

void PowerManager::timerCallback(const ros::TimerEvent&) {
  const std::lock_guard<std::mutex> lg(pin_mutex_);

  apl_msgs::PowerStatus msg;
  for (const std::string& channel : channels_) {
    int wiringpi_pin = wiringpi_pins_[channel];
    int pin_status = digitalRead(wiringpi_pin);
    msg.header.stamp = ros::Time::now();
    msg.pins.push_back(wiringpi_pin);
    msg.channels.push_back(channel);
    if (on_is_high_[channel]) {
      if (pin_status == 0) {
        msg.status.push_back(msg.OFF);
      } else {
        msg.status.push_back(msg.ON);
      }
    } else {
      if (pin_status == 0) {
        msg.status.push_back(msg.ON);
      } else {
        msg.status.push_back(msg.OFF);
      }
    }
  }
  status_pub_.publish(msg);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "power_switch_driver");
  PowerManager pd;
  pd.run();
  return 0;
}
